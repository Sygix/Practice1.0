package com.sygix.Practice.Utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public interface CustomInventory {

    public abstract String getName();

    public abstract void addPlayer(Player player);

    public abstract void contents(Player player, Inventory inv);

    public abstract void onClick(Player player, Inventory inv, ItemStack current, int slot);

    public abstract int getSize();

}