package com.sygix.Practice.Utils;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 07/03/2017
 *******************************************************************************/
public class ArgsUtil {

    public static String getArgsFromStringList(String[] args, int x)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = x; i < args.length; i++)
        {
            sb.append(args[i]).append( i >= args.length - 1 ? "" : " " );
        }
        return sb.toString();
    }

}
