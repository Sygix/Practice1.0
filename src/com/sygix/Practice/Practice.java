package com.sygix.Practice;

import com.sygix.Practice.Commands.*;
import com.sygix.Practice.Events.EventManager;
import com.sygix.Practice.Inventories.KitsMenu;
import com.sygix.Practice.Inventories.PlayerInventoryResume;
import com.sygix.Practice.Tasks.QueueTask;
import com.sygix.Practice.Utils.CustomInventory;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 26/02/2017
 *******************************************************************************/
public class Practice extends JavaPlugin{

    public static Practice instance;
    public static FileConfiguration config;
    public static FileConfiguration kits;
    private static File file;
    public static ArrayList<Player> onlinePlayers = new ArrayList<>();
    public static List<String> chestList = new ArrayList();
    public static Map<Class<? extends CustomInventory>, CustomInventory > registeredMenus = new HashMap<>();

    public static ArrayList<Player> waitingPlayer = new ArrayList<>();
    public static ConcurrentHashMap<Player, Player> waitingDuels = new ConcurrentHashMap<>();

    public static ArrayList<String> freeArena = new ArrayList<>();
    public static ArrayList<String> usedArena = new ArrayList<>();

    public static HashMap<String, Integer> areneTask = new HashMap();
    public static HashMap<Player, String> playerArene = new HashMap();
    public static HashMap<String, Integer> countdown = new HashMap();
    public static HashMap<String, Integer> time = new HashMap();

    public static Practice getInstance() {
        return instance;
    }

    @Override
    public void onEnable(){
        System.out.println("--Chargement du plugin "+Bukkit.getPluginManager().getPlugin("Practice1.0").getDescription().getVersion()
                +" by "+Bukkit.getPluginManager().getPlugin("Practice1.0").getDescription().getAuthors()+"--");
        /*Code*/
        if (!Bukkit.getVersion().contains("1.7.10")){
            System.out.println("--Ce plugin est fait pour fonctionner avec Spigot-1.7.10--");
        }
        instance = this;
        if(!this.getDataFolder().exists()){
            this.getDataFolder().mkdir();
        }
        file = new File(this.getDataFolder().getPath(), "kits.yml");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        kits = YamlConfiguration.loadConfiguration(file);
        this.saveDefaultConfig();
        config = this.getConfig();
        getCommand("duel").setExecutor(new Duel());
        getCommand("match").setExecutor(new Match());
        getCommand("arene").setExecutor(new Arene());
        getCommand("practice").setExecutor(new PracticeCmd());
        getCommand("chest").setExecutor(new ChestCmd());
        addMenu(new KitsMenu());
        addMenu(new PlayerInventoryResume());
        EventManager.registerEvent(this);
        loadArenes();
        loadChests();
        QueueTask.DuelsQueueTaskLaunch();
        QueueTask.MatchQueueTaskLaunch();
        /*End*/
        System.out.println("--Plugin charge--");
    }

    @Override
    public void onDisable(){
        System.out.println("--Dechargement du plugin "+Bukkit.getPluginManager().getPlugin("Practice1.0").getDescription().getVersion()
                +" by "+Bukkit.getPluginManager().getPlugin("Practice1.0").getDescription().getAuthors()+"--");
        /*Code*/

        /*End*/
        System.out.println("--Plugin decharge--");
    }

    public void reloadConfiguration(){
        this.reloadConfig();
        config = this.getConfig();
    }

    public static void loadArenes(){
        try{
            config.getConfigurationSection("Arene").getKeys(false).forEach(arene -> {
                freeArena.add(arene);
                System.out.println("Arene "+arene+" loaded !");
            });
        }catch (Exception e){
            System.out.println("Aucune arene a charger !");
        }
    }

    public static void loadChests(){
        try{
            config.getList("Chest").forEach(chest -> {
                chestList.add(chest.toString());
                System.out.println("Coffre "+chest+" loaded !");
            });
        }catch (Exception e){
            System.out.println("Aucun Coffre a charger !");
        }
    }

    public static List<Location> getChestListLocation(){
        List<Location> list = new ArrayList<>();
        chestList.forEach(chest -> {
            String[] chests = chest.split(":");
            list.add(new Location(Bukkit.getWorld(chests[0]), Integer.parseInt(chests[1]), Integer.parseInt(chests[2]), Integer.parseInt(chests[3])));
        });
        return list;
    }

    public static void saveKits(){
        try {
            kits.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addMenu(CustomInventory m){
        this.registeredMenus.put(m.getClass(), m);
    }

    /*public static void open(Player player, Class<? extends CustomInventory> gClass){

        if(!registeredMenus.containsKey(gClass)) return;

        CustomInventory menu = registeredMenus.get(gClass);
        Inventory inv = Bukkit.createInventory(null, menu.getSize(), menu.getName());
        menu.contents(player, inv);
        player.openInventory(inv);

    }*/

    public static void open(Player player, Player player2, Class<? extends CustomInventory> gClass){

        if(!registeredMenus.containsKey(gClass)) return;

        CustomInventory menu = registeredMenus.get(gClass);
        menu.addPlayer(player2);
        Inventory inv = Bukkit.createInventory(null, menu.getSize(), menu.getName());
        menu.contents(player, inv);
        player.openInventory(inv);

    }

}
