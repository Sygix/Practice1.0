package com.sygix.Practice.Inventories;

import com.sygix.Practice.Utils.CustomInventory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.Arrays;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class PlayerInventoryResume implements CustomInventory {

    private Player player2;

    @Override
    public String getName() {
        if(this.player2 == null){
            return null;
        }
        return "§cInventaire de "+player2.getDisplayName();
    }

    @Override
    public void addPlayer(Player player) {
        this.player2 = player;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void contents(Player player, Inventory inv) {
        inv.setContents(player2.getInventory().getContents());

        inv.setItem(45, player2.getInventory().getHelmet());
        inv.setItem(46, player2.getInventory().getChestplate());
        inv.setItem(47, player2.getInventory().getLeggings());
        inv.setItem(48, player2.getInventory().getBoots());

        ItemStack i = new ItemStack(Material.APPLE);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName("§cVie :");
        im.setLore(Arrays.asList("Niveau : "+((Double) Double.parseDouble(player2.getHealth()+"")).intValue()));
        i.setItemMeta(im);

        ItemStack i1 = new ItemStack(Material.COOKED_BEEF);
        ItemMeta im1 = i1.getItemMeta();
        im1.setDisplayName("§aNourriture :");
        im1.setLore(Arrays.asList("Niveau : "+player2.getFoodLevel()));
        i1.setItemMeta(im1);

        ItemStack i2 = new ItemStack(Material.POTION);
        ItemMeta im2 = i2.getItemMeta();
        im2.setDisplayName("§6Effet(s) de potion :");
        ArrayList<String> list = new ArrayList<>();
        for(PotionEffect pe : player2.getActivePotionEffects()){
            list.add(pe.getType().getName()+" : "+Integer.parseInt(pe.getAmplifier()+1+""));
        }
        im2.setLore(list);
        i2.setItemMeta(im2);

        inv.setItem(51, i);
        inv.setItem(52, i1);
        inv.setItem(53, i2);
    }

    @Override
    public void onClick(Player player, Inventory inv, ItemStack current, int slot) {
        return;
    }

    @Override
    public int getSize() {
        return 54;
    }
}
