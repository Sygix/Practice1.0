package com.sygix.Practice.Inventories;

import com.sygix.Practice.Practice;
import com.sygix.Practice.Utils.BukkitSerialization;
import com.sygix.Practice.Utils.CustomInventory;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.util.Arrays;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class KitsMenu implements CustomInventory {


    @Override
    public String getName() {
        return "§6Kits";
    }

    @Override
    public void addPlayer(Player player) {

    }

    @Override
    public void contents(Player player, Inventory inv) {
        ItemStack i = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName("§aKit - 1");
        im.setLore(Arrays.asList("§aPermet de charger le kit.", "§5§oVous devez en avoir enregistrer un préalablement."));
        i.setItemMeta(im);

        ItemStack i1 = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta im1 = i1.getItemMeta();
        im1.setDisplayName("§aKit - 2");
        im1.setLore(Arrays.asList("§aPermet de charger le kit.", "§5§oVous devez en avoir enregistrer un préalablement."));
        i1.setItemMeta(im1);

        ItemStack i2 = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta im2 = i2.getItemMeta();
        im2.setDisplayName("§aKit - 3");
        im2.setLore(Arrays.asList("§aPermet de charger le kit.", "§5§oVous devez en avoir enregistrer un préalablement."));
        i2.setItemMeta(im2);

        ItemStack i3 = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta im3 = i3.getItemMeta();
        im3.setDisplayName("§cKit - 1");
        im3.setLore(Arrays.asList("§aPermet d'enregistrer le kit.", "§5§oCela remplacera le kit précédent."));
        i3.setItemMeta(im3);

        ItemStack i4 = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta im4 = i4.getItemMeta();
        im4.setDisplayName("§cKit - 2");
        im4.setLore(Arrays.asList("§aPermet d'enregistrer le kit.", "§5§oCela remplacera le kit précédent."));
        i4.setItemMeta(im4);

        ItemStack i5 = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta im5 = i5.getItemMeta();
        im5.setDisplayName("§cKit - 3");
        im5.setLore(Arrays.asList("§aPermet d'enregistrer le kit.", "§5§oCela remplacera le kit précédent."));
        i5.setItemMeta(im5);

        inv.setItem(2, i);
        inv.setItem(4, i1);
        inv.setItem(6, i2);
        inv.setItem(20, i3);
        inv.setItem(22, i4);
        inv.setItem(24, i5);
    }

    @Override
    public void onClick(Player p, Inventory inv, ItemStack current, int slot) {
        String name = ChatColor.stripColor(current.getItemMeta().getDisplayName());
        if(current.getType().equals(Material.DIAMOND_SWORD)){
            if(Practice.kits.isSet(p.getUniqueId()+"."+name) == true){
                try{
                    p.getInventory().clear();
                    p.getInventory().setArmorContents(BukkitSerialization.itemStackArrayFromBase64(Practice.kits.getString(p.getUniqueId()+"."+name+".ArmorContents")));
                    p.getInventory().setContents(BukkitSerialization.itemStackArrayFromBase64(Practice.kits.getString(p.getUniqueId()+"."+name+".Contents")));
                    p.updateInventory();
                    p.sendMessage("§aKit chargé.");
                    p.closeInventory();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        }else if(current.getType().equals(Material.DIAMOND_CHESTPLATE)){
            Practice.kits.set(p.getUniqueId()+"."+name+".ArmorContents", BukkitSerialization.itemStackArrayToBase64(p.getInventory().getArmorContents()));
            Practice.kits.set(p.getUniqueId()+"."+name+".Contents", BukkitSerialization.itemStackArrayToBase64(p.getInventory().getContents()));
            Practice.saveKits();
            p.sendMessage("§aKit sauvegardé.");
            p.closeInventory();
        }

    }

    @Override
    public int getSize() {
        return 27;
    }
}
