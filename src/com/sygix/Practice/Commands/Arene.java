package com.sygix.Practice.Commands;

import com.sygix.Practice.Practice;
import com.sygix.Practice.Utils.ArgsUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class Arene implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player){
            FileConfiguration config = Practice.config;
            Player p = (Player) sender;
            if(args.length >= 2){
                if(args[0].equalsIgnoreCase("delete")) {
                    if (config.getConfigurationSection("Arene").getKeys(false).contains(args[1].toLowerCase())) {
                        config.set("Arene." + args[1].toLowerCase(), null);
                        p.sendMessage("§aArène supprimé.");
                    } else {
                        p.sendMessage("§cCette arène n'existe pas.");
                    }
                }
            }else if(args.length < 2 && args.length > 0){
                if(args[0].equalsIgnoreCase("save")){
                    Practice.getInstance().saveConfig();
                    p.sendMessage("§aArènes sauvegardées.");
                } else if(args[0].equalsIgnoreCase("reload")){
                    Practice.getInstance().reloadConfiguration();
                    Practice.loadArenes();
                    p.sendMessage("§aFichier de configuration rechargé.");
                }else{
                    try{
                        if(!config.getConfigurationSection("Arene").getKeys(false).contains(args[0].toLowerCase())){
                            config.set("Arene."+args[0].toLowerCase(), "");
                            p.sendMessage("§aArène créé !");
                        }
                    }catch (Exception e){
                        config.set("Arene."+args[0].toLowerCase(), "");
                        p.sendMessage("§aArène créé !");
                    }
                    int spawns;
                    try{
                        spawns = config.getConfigurationSection("Arene."+args[0].toLowerCase()).getKeys(false).size();
                    }catch (Exception e){
                        spawns = 0;
                    }
                    String[] location = {p.getWorld().getName(), p.getLocation().getX()+"", p.getLocation().getY()+"", p.getLocation().getZ()+"",
                            p.getEyeLocation().getYaw()+"", p.getEyeLocation().getPitch()+""};
                    String loc = ArgsUtil.getArgsFromStringList(location, 0).replace(' ', ':');
                    if(spawns >= 2){
                        p.sendMessage("§cVous avez déjà créé assez de spawn.");
                    }else if(spawns == 0){
                        config.set("Arene."+args[0].toLowerCase()+".1", loc);
                        p.sendMessage("§aSpawn ajouté.");
                    }else if(spawns == 1){
                        config.set("Arene."+args[0].toLowerCase()+".2", loc);
                        p.sendMessage("§aSpawn ajouté.");
                    }
                }
            }else{
                p.sendMessage("§7------------------------------------------\n" +
                        "    §7- /arene : §aAide.\n" +
                        "    §7- /arene <nom> : §aCréer et/ou ajoute un spawn.\n" +
                        "    §7- /arene delete <nom> : §aSupprime une arène.\n" +
                        "    §7- /arene save : §aSauvegarde les arènes.\n" +
                        "    §7- /arene reload : §aRecharge le fichier de configuration.\n" +
                        "§7------------------------------------------");
            }
        }
        return false;
    }
}
