package com.sygix.Practice.Commands;

import com.sygix.Practice.Practice;
import com.sygix.Practice.Utils.HashMapUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 26/02/2017
 *******************************************************************************/
public class Duel implements CommandExecutor{

    public static HashMap<Player , Player> requests = new HashMap<>(); // Key, Target

    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if (args.length == 1) {
                if(Practice.playerArene.containsKey(p)){return false;}
                if(Practice.waitingPlayer.contains(p)){
                    Practice.waitingPlayer.remove(p);
                }
                if (args[0].equalsIgnoreCase("accept")) {
                    Player key = HashMapUtils.getKeyByValue(requests, p);
                    Practice.waitingDuels.put(key, p);
                    requests.remove(key);
                    key.sendMessage("§a"+p.getName()+" §6a accepté votre invitation, vous avez été placé en file d'attente.");
                    p.sendMessage("§6Vous avez accepté le duel, vous avez été placé en file d'attente.");
                }else if (args[0].equalsIgnoreCase("cancel")) {
                    if (requests.containsKey(p)) {
                        requests.get(p).sendMessage("§cVotre duel a été annulé.");
                        requests.remove(p);
                        p.sendMessage("§6Vous avez annulé le duel.");
                    } else if(requests.containsValue(p)) {
                        Player key = HashMapUtils.getKeyByValue(requests, p);
                        requests.remove(key, p);
                        key.sendMessage("§cVotre demande de duel a été refusée.");
                        p.sendMessage("§cVous avez refusé le duel.");
                    } else {
                        p.sendMessage("§cVous n'avez pas de duel en attente.");
                    }
                } else if(Practice.onlinePlayers.contains(Bukkit.getPlayer(args[0])) && Bukkit.getPlayer(args[0]) != p ){
                    Player target = Bukkit.getPlayer(args[0]);
                    requests.put(p, target);
                    TextComponent message = new TextComponent("Vous avez été défié par " + p.getName() + " !");
                    message.setColor(ChatColor.GOLD);
                    message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel accept"));
                    message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Clique pour accepter le duel !").create()));
                    target.spigot().sendMessage(message);
                    TextComponent message1 = new TextComponent("Vous venez de défier " + target.getName() + " !");
                    message1.setColor(ChatColor.GOLD);
                    message1.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel cancel"));
                    message1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Clique pour annuler le duel !").create()));
                    p.spigot().sendMessage(message1);
                    Bukkit.getScheduler().runTaskLater(Practice.getInstance(), () -> {
                        if(requests.containsKey(p)){
                            requests.remove(p, target);
                            p.sendMessage("§cVotre invitation n'a pas été accepté, elle a donc expiré.");
                            target.sendMessage("§cL'invitation lancé par "+p.getName()+" a expiré.");
                        }
                    }, 20*15);
                }else{
                    p.sendMessage("§cCe joueur n'est pas en ligne/ne peut pas être vous même.");
                }
            }else{
            sender.sendMessage("§7------------------------------------------\n" +
                    "    §7- /duel : §aAide.\n" +
                    "    §7- /duel <Joueur> : §aDemander un duel.\n" +
                    "    §7- /duel accept : §aAccepte un duel qui vous a été lancé.\n" +
                    "    §7- /duel cancel : §aSupprime une demande de duel.\n" +
                    "§7------------------------------------------");
            }
        }
        return false;
    }

}