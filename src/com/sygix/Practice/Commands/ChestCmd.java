package com.sygix.Practice.Commands;

import com.sygix.Practice.Practice;
import com.sygix.Practice.Utils.ArgsUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class ChestCmd implements CommandExecutor {
    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(sender instanceof Player){
            Player p = (Player)sender;
            if(p.getTargetBlock(null, 10).getType().equals(Material.CHEST) || p.getTargetBlock(null, 10).getType().equals(Material.ENDER_CHEST)){
                String[] location = {p.getTargetBlock(null, 10).getWorld().getName(),
                        p.getTargetBlock(null, 10).getLocation().getBlockX()+"",
                        p.getTargetBlock(null, 10).getLocation().getBlockY()+"",
                        p.getTargetBlock(null, 10).getLocation().getBlockZ()+""};
                String loc = ArgsUtil.getArgsFromStringList(location, 0).replace(' ', ':');
                if(Practice.chestList.contains(loc)){
                    Practice.chestList.remove(loc);
                    Practice.config.set("Chest", Practice.chestList);
                    Practice.getInstance().saveConfig();
                    p.sendMessage("§cCoffre supprimé.");
                }else{
                    Practice.chestList.add(loc);
                    Practice.config.set("Chest", Practice.chestList);
                    Practice.getInstance().saveConfig();
                    p.sendMessage("§aCoffre ajouté.");
                }
            }else{
                p.sendMessage("§cVous ne visez pas de coffre à moins de 10 blocs.");
            }
        }

        return false;
    }
}
