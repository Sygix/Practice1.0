package com.sygix.Practice.Commands;

import com.sygix.Practice.Practice;
import com.sygix.Practice.Utils.ArgsUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class PracticeCmd implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(sender instanceof Player){
            Player p = (Player) sender;
            String[] location = {p.getWorld().getName(), p.getLocation().getX()+"", p.getLocation().getY()+"", p.getLocation().getZ()+"",
                    p.getEyeLocation().getYaw()+"", p.getEyeLocation().getPitch()+""};
            String loc = ArgsUtil.getArgsFromStringList(location, 0).replace(' ', ':');
            Practice.config.set("Configuration.Spawn", loc);
            Practice.getInstance().saveConfig();
            p.sendMessage("§aSpawn défini.");
        }

        return false;
    }
}
