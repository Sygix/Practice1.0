package com.sygix.Practice.Commands;

import com.sygix.Practice.Practice;
import com.sygix.Practice.Utils.HashMapUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class Match implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player){
            Player p = (Player) sender;
            if(Practice.playerArene.containsKey(p)){return false;}
            if(Practice.waitingDuels.containsKey(p)){ //KEY QUIT DUEL
                Player value = Practice.waitingDuels.get(p);
                Practice.waitingPlayer.add(value);
                value.sendMessage("§cVotre partenaire a quitté votre duel, vous avec été placé dans la file d'attente solo.");
                Practice.waitingDuels.remove(p);
            }
            if(Practice.waitingDuels.containsValue(p)){ //VALUE QUIT DUEL
                Player key = HashMapUtils.getKeyByValue(Practice.waitingDuels, p);
                Practice.waitingPlayer.add(key);
                key.sendMessage("§cVotre partenaire a quitté votre duel, vous avec été placé dans la file d'attente solo.");
                Practice.waitingDuels.remove(key);
            }
            if(!Practice.waitingPlayer.contains(p)){
                TextComponent message = new TextComponent("Vous avez été ajouté dans la file d'attente !");
                message.setColor(ChatColor.GOLD);
                message.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/match" ));
                message.setHoverEvent( new HoverEvent( HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Clique pour te retirer de la file d'attente !").create() ) );
                p.spigot().sendMessage(message);
                Practice.waitingPlayer.add(p);
            }else{
                TextComponent message = new TextComponent("Vous avez été retiré de la file d'attente !");
                message.setColor(ChatColor.GOLD);
                message.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/match" ));
                message.setHoverEvent( new HoverEvent( HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Clique pour t'ajouter dans la file d'attente !").create() ) );
                p.spigot().sendMessage(message);
                Practice.waitingPlayer.remove(p);
            }
        }
        return false;
    }


}
