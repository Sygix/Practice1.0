package com.sygix.Practice.Events;

import com.sygix.Practice.Commands.Duel;
import com.sygix.Practice.Practice;
import com.sygix.Practice.Utils.HashMapUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class PlayerQuit implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        Player p = e.getPlayer();
        Practice.onlinePlayers.remove(p);
        Duel.requests.remove(p);
        if(Practice.waitingDuels.containsKey(p)){ //KEY DECO VALUE CHANGE WAITING LIST
            Player value = Practice.waitingDuels.get(p);
            Practice.waitingPlayer.add(value);
            value.sendMessage("§cVotre partenaire s'est déconnecté, vous avec été placé dans la file d'attente solo.");
            Practice.waitingDuels.remove(p);
        }
        if(Practice.waitingDuels.containsValue(p)){ //VALUE DECO KEY CHANGE WAITING LIST
            Player key = HashMapUtils.getKeyByValue(Practice.waitingDuels, p);
            Practice.waitingPlayer.add(key);
            key.sendMessage("§cVotre partenaire s'est déconnecté, vous avec été placé dans la file d'attente solo.");
            Practice.waitingDuels.remove(key);
        }
        if(Practice.playerArene.containsKey(p)){
            String arene = Practice.playerArene.get(p);
            Practice.time.replace(arene, 0);
            String[] spawn = Practice.config.getString("Configuration.Spawn").split(":");
            Location spawnloc = new Location(Bukkit.getWorld(spawn[0]), Double.parseDouble(spawn[1]), Double.parseDouble(spawn[2]), Double.parseDouble(spawn[3]),
                    Float.parseFloat(spawn[4]), Float.parseFloat(spawn[5]));
            p.teleport(spawnloc);
        }
    }

}
