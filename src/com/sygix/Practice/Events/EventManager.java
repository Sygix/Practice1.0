package com.sygix.Practice.Events;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 26/02/2017
 *******************************************************************************/
public class EventManager {

    public static void registerEvent(Plugin pl){
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerJoin(), pl);
        pm.registerEvents(new PlayerQuit(), pl);
        pm.registerEvents(new PlayerDamage(), pl);
        pm.registerEvents(new PlayerInteract(), pl);
        pm.registerEvents(new InventoryClick(), pl);
        pm.registerEvents(new BlockBreak(), pl);
    }

}
