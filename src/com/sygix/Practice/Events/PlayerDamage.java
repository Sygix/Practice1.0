package com.sygix.Practice.Events;

import com.sygix.Practice.Practice;
import com.sygix.Practice.Tasks.MatchTask;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class PlayerDamage implements Listener {

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e){
        if ((e.getEntity() instanceof Player)) {
            Player p = (Player)e.getEntity();
            if(Practice.playerArene.containsKey(p)){
                if (p.getHealth() <= e.getFinalDamage()) {
                    String arene = Practice.playerArene.get(p);
                    e.setDamage(0.0);
                    p.sendMessage("§cVous êtes mort");
                    p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 255));
                    Practice.time.replace(arene, 0);
                }
            }
        }
    }

}
