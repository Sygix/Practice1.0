package com.sygix.Practice.Events;

import com.sygix.Practice.Practice;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class InventoryClick implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e){
        Player player = (Player) e.getWhoClicked();
        Inventory inv = e.getInventory();
        ItemStack current = e.getCurrentItem();

        if(e.getCurrentItem() == null) return;
        if(e.getCurrentItem().getType().equals(Material.AIR))return;

        Practice.registeredMenus.values().stream()
                .filter(menu -> inv.getName().equalsIgnoreCase(menu.getName()))
                .forEach(menu -> {
                    menu.onClick(player, inv, current, e.getSlot());
                    e.setCancelled(true);
                });
    }

}
