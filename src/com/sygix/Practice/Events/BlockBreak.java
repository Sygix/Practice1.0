package com.sygix.Practice.Events;

import com.sygix.Practice.Practice;
import com.sygix.Practice.Utils.ArgsUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class BlockBreak implements Listener {

    @EventHandler
    public void onBreakBlock(BlockBreakEvent e){
        Block b = e.getBlock();
        Player p = e.getPlayer();
        if(b.getType().equals(Material.CHEST) || b.getType().equals(Material.ENDER_CHEST)){
            if(Practice.getChestListLocation().contains(b.getLocation())){
                String[] location = {b.getWorld().getName(),
                        b.getLocation().getBlockX()+"",
                        b.getLocation().getBlockY()+"",
                        b.getLocation().getBlockZ()+""};
                String loc = ArgsUtil.getArgsFromStringList(location, 0).replace(' ', ':');
                Practice.chestList.remove(loc);
                Practice.config.set("Chest", Practice.chestList);
                Practice.getInstance().saveConfig();
                p.sendMessage("§cCoffre supprimé.");
            }
        }
    }

}
