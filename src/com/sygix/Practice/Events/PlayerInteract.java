package com.sygix.Practice.Events;

import com.sygix.Practice.Inventories.KitsMenu;
import com.sygix.Practice.Practice;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 03/03/2017
 *******************************************************************************/
public class PlayerInteract implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e){
        Player p = e.getPlayer();
        if(e.getClickedBlock() != null){
            Block b = e.getClickedBlock();
            if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                if(b.getType().equals(Material.CHEST) && Practice.getChestListLocation().contains(b.getLocation())){
                    if(p.hasPermission("Practice.modifyChest")){
                        if(p.isSneaking()){
                            p.sendMessage("§cVous modifier le coffre.");
                            return;
                        }
                    }
                    Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST);
                    inv.setContents(((Chest) b.getState()).getInventory().getContents());
                    p.openInventory(inv);
                    e.setCancelled(true);
                }
                if(b.getType().equals(Material.ENDER_CHEST) && Practice.getChestListLocation().contains(b.getLocation())){
                    Practice.open(p, null, KitsMenu.class);
                    e.setCancelled(true);
                }
            }
        }
    }

}
