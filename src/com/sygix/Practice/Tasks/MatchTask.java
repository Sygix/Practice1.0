package com.sygix.Practice.Tasks;

import com.sygix.Practice.Inventories.PlayerInventoryResume;
import com.sygix.Practice.Practice;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 07/03/2017
 *******************************************************************************/
public class MatchTask {

    @SuppressWarnings("deprecation")
    public static void startTask(String arene, Player p1, Player p2){
        if(Practice.areneTask.containsKey(arene)){
            return;
        }

        FileConfiguration config = Practice.config;
        String[] location1 = config.getString("Arene."+arene+".1").split(":"); //world:X:Y:Z:Yaw:Pitch
        String[] location2 = config.getString("Arene."+arene+".2").split(":");
        Location spawn1 = new Location(Bukkit.getWorld(location1[0]), Double.parseDouble(location1[1]), Double.parseDouble(location1[2]), Double.parseDouble(location1[3]),
                Float.parseFloat(location1[4]), Float.parseFloat(location1[5]));
        Location spawn2 = new Location(Bukkit.getWorld(location2[0]), Double.parseDouble(location2[1]), Double.parseDouble(location2[2]), Double.parseDouble(location2[3]),
                Float.parseFloat(location2[4]), Float.parseFloat(location2[5]));

        Practice.playerArene.put(p1, arene);
        Practice.playerArene.put(p2, arene);
        Practice.countdown.put(arene, Practice.config.getInt("Configuration.Countdown"));
        Practice.time.put(arene, Practice.config.getInt("Configuration.Maxtime"));

        Practice.areneTask.put(arene, Bukkit.getScheduler().scheduleSyncRepeatingTask(Practice.getInstance(), () -> {
            if(Practice.time.get(arene) < Practice.config.getInt("Configuration.Maxtime")){
                if(Practice.time.get(arene) == 30 || Practice.time.get(arene) == 15 || Practice.time.get(arene) <= 5){
                    p1.playSound(p1.getLocation(), Sound.ORB_PICKUP, 1F, 1F);
                    p2.playSound(p1.getLocation(), Sound.ORB_PICKUP, 1F, 1F);
                    p1.sendMessage("§cIl reste "+Practice.time.get(arene)+" seconde(s).");
                    p2.sendMessage("§cIl reste "+Practice.time.get(arene)+" seconde(s).");
                }
                if(Practice.time.get(arene) <= 0){
                    Bukkit.getScheduler().cancelTask(Practice.areneTask.get(arene));
                    Practice.areneTask.remove(arene);
                    Practice.time.remove(arene);
                    Practice.countdown.remove(arene);
                    Practice.playerArene.remove(p1);
                    Practice.playerArene.remove(p2);
                    String[] spawn = config.getString("Configuration.Spawn").split(":");
                    Location spawnloc = new Location(Bukkit.getWorld(spawn[0]), Double.parseDouble(spawn[1]), Double.parseDouble(spawn[2]), Double.parseDouble(spawn[3]),
                            Float.parseFloat(spawn[4]), Float.parseFloat(spawn[5]));
                    p1.teleport(spawnloc);
                    p2.teleport(spawnloc);
                    p1.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                    p2.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                    Practice.open(p1, p2, PlayerInventoryResume.class);
                    Practice.open(p2, p1, PlayerInventoryResume.class);
                    for(PotionEffect pe : p1.getActivePotionEffects()){
                        p1.removePotionEffect(pe.getType());
                    }
                    for(PotionEffect pe : p2.getActivePotionEffects()){
                        p2.removePotionEffect(pe.getType());
                    }
                    p1.setHealth(20);
                    p1.setFoodLevel(20);
                    p2.setHealth(20);
                    p2.setFoodLevel(20);
                    Practice.usedArena.remove(arene);
                    Practice.freeArena.add(arene);
                    return;
                }
                Practice.time.replace(arene, Practice.time.get(arene)-1);
            }else{
                if(Practice.countdown.get(arene) <= 0){
                    p1.sendMessage("\n§cCombat !\n ");
                    p2.sendMessage("\n§cCombat !\n ");
                    if(Practice.time.get(arene) != -1){
                        Practice.time.replace(arene, Practice.time.get(arene)-1);
                    }
                }else{
                    p1.teleport(spawn1);
                    p2.teleport(spawn2);
                    if(Practice.countdown.get(arene) <= 5){
                        p1.playSound(p1.getLocation(), Sound.ORB_PICKUP, 1F, 1F);
                        p2.playSound(p1.getLocation(), Sound.ORB_PICKUP, 1F, 1F);
                        p1.sendMessage("§aDébut du combat dans : §5"+Practice.countdown.get(arene)+" seconde(s)");
                        p2.sendMessage("§aDébut du combat dans : §5"+Practice.countdown.get(arene)+" seconde(s)");
                    }
                    Practice.countdown.replace(arene, Practice.countdown.get(arene)-1);
                }
            }
        }, 20, 20));
    }

}
