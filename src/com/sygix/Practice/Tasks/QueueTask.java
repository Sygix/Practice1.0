package com.sygix.Practice.Tasks;

import com.sygix.Practice.Practice;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map;

/*******************************************************************************
 * Code by Sygix.
 * http://sygix.tk/
 * Created the 06/03/2017
 *******************************************************************************/
public class QueueTask {

    //AVANT LE /MATCH ET /DUEL VERIF SI IL EST DEJA DANS UNE LISTE D ATTENTE

    public static int duelsqueuetask;
    public static boolean duelsqueuerun = false;
    public static int matchqueuetask;
    public static boolean matchqueuerun = false;

    public static void DuelsQueueTaskLaunch(){
        if(duelsqueuerun == true){
            return;
        }

        duelsqueuerun = true;
        duelsqueuetask = Bukkit.getScheduler().scheduleSyncRepeatingTask(Practice.getInstance(), () -> {
            //CHECK DES ARENES + TP
            if(Practice.waitingDuels.size() >= 1){
                Practice.waitingDuels.forEach(( K , V ) -> {
                    if(Practice.freeArena.size() >= 1){
                        String arene = Practice.freeArena.get(0);
                        Practice.freeArena.remove(arene);
                        Practice.usedArena.add(arene);
                        MatchTask.startTask(arene, K, V);
                        Practice.waitingDuels.remove(K, V);
                    }
                });
            }

        }, 20, 20);
    }

    public static void MatchQueueTaskLaunch(){
        if(matchqueuerun == true){
            return;
        }

        matchqueuerun = true;
        matchqueuetask = Bukkit.getScheduler().scheduleSyncRepeatingTask(Practice.getInstance(), () -> {
            //CHECK DES ARENES + TP
            if(Practice.waitingPlayer.size() >= 2){ //CHECK SI IL Y A AU MOINS DEUX JOUEURS
                if(Practice.freeArena.size() >= 1){
                    String arene = Practice.freeArena.get(0);
                    Player p1 = Practice.waitingPlayer.get(0);
                    Player p2 = Practice.waitingPlayer.get(1);
                    Practice.freeArena.remove(arene);
                    Practice.usedArena.add(arene);
                    MatchTask.startTask(arene, p1, p2);
                    Practice.waitingPlayer.remove(p1);
                    Practice.waitingPlayer.remove(p2);
                }
            }

        }, 20, 20);
    }

}
